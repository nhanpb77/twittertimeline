//
//  TimelineViewController.swift
//  TwitterTimeline
//
//  Created by Nhan Phung on 11/27/16.
//  Copyright © 2016 Nhan Phung. All rights reserved.
//

import UIKit

class TimelineViewController: UIViewController, TwitterOauth2Delegate, UITableViewDataSource {

    @IBOutlet weak var tweetTableView: UITableView!
    
    var tweetList: [Tweet] = [Tweet]()
    let consumerKey: String = "502cwhWFzOgmxEwsvwiYgGUKX"
    let consumerSecretKey: String = "vkqTBNcGmTiYvyQAOjFXPHgzJz3acLq3qo5FPnBLINnLkrrbl8"
    let oAuth2URL: String = "https://api.twitter.com/oauth2/token"
    var userTimelineURL : String = "https://api.twitter.com/1.1/statuses/user_timeline.json"
    var accessToken : String = String()
    var twitter : Twitter?
    var info: (user: String,count: String)?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        userTimelineURL = String.init(format: "%@?screen_name=%@&count=%@", userTimelineURL,(info?.user)!,(info?.count)!)
        tweetTableView.dataSource = self
        
        twitter = Twitter(consumerKey, consumerSecretKey, oAuth2URL)
        twitter?.delegate = self
        twitter?.requestToken()
}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func getAccessToken(_ accessToken: String) {
        twitter!.requestTimeLine(accessToken, userTimelineURL,{
        tweetArray in
            DispatchQueue.main.async {
                self.tweetList = tweetArray
                self.tweetTableView .reloadData()
            }
            
        })
    }
    
    //MARK: - TableView Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tweetList.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellID = "cell"
        let cell  = (tweetTableView.dequeueReusableCell(withIdentifier: cellID) as! TimelineTableViewCell)
        let tweet : Tweet = tweetList[indexPath.row]

        let nameString : String = String.init(format: "%@ @%@", tweet.name,tweet.screenName)
        let rangeName = (nameString as NSString).range(of: String.init(format: "@%@", tweet.screenName))
        let attribute = NSMutableAttributedString.init(string: nameString)
        attribute.addAttribute(NSForegroundColorAttributeName, value: UIColor.lightGray, range: rangeName)
        attribute.addAttribute(NSFontAttributeName, value: UIFont.systemFont(ofSize: 14), range: rangeName)
        cell.nameLabel.attributedText = attribute

        cell.descriptionLabel.text = tweet.descriptionText
        cell.profileImage.image = try! UIImage(data: Data(contentsOf: URL(string: tweet.profileImageURL)!))
        cell.profileImage.layer.cornerRadius = 10.0
        cell.profileImage.clipsToBounds = true
        return cell
    }
    
}
