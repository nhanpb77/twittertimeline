//
//  Tweet.swift
//  TwitterTimeline
//
//  Created by Nhan Phung on 11/28/16.
//  Copyright © 2016 Nhan Phung. All rights reserved.
//

import UIKit

class Tweet: NSObject {
    
    var name: String
    var screenName: String
    var profileImageURL: String
    var descriptionText: String
    
    init(_ name: String,_ screenName: String,_ profileImageURL: String,_ descriptionText: String) {
        self.name = name
        self.screenName = screenName
        self.profileImageURL = profileImageURL
        self.descriptionText = descriptionText
        
        super.init()
    }
}
