//
//  Twitter.swift
//  TwitterTimeline
//
//  Created by Nhan Phung on 11/27/16.
//  Copyright © 2016 Nhan Phung. All rights reserved.
//

import UIKit
protocol TwitterOauth2Delegate {
    
    func getAccessToken(_ accessToken: String)
}

class Twitter: NSObject {
    
    let consumerKey: String
    let consumerSecretKey: String
    let oAuth2URL: String
    
    var delegate : TwitterOauth2Delegate?
    var tweetList: [Tweet] = [Tweet]()
    
    init(_ consumerKey: String,_ consumerSecretKey: String,_ oAuth2URL: String) {
        self.consumerKey = consumerKey
        self.consumerSecretKey = consumerSecretKey
        self.oAuth2URL = oAuth2URL
        
        super.init()
    }
    
    public func requestToken(){
        var accessToken : String = String()
        
        let bearerToken : String = String.init(format:"%@:%@", consumerKey.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!,consumerSecretKey.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)
        let plainBearrerToken : Data = bearerToken.data(using: String.Encoding.utf8)!
        
        let bearrerTokenBase64 : String = String(describing: plainBearrerToken.base64EncodedString())
        
        let request : NSMutableURLRequest = NSMutableURLRequest()
        request.httpMethod = "POST"
        request.url = URL(string: oAuth2URL)!
        
        request.addValue(NSString.init(format: "Basic %@", bearrerTokenBase64) as String, forHTTPHeaderField:"Authorization")
        request.addValue("application/x-www-form-urlencoded;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        
        let bodyString : String = "grant_type=client_credentials"
        request.httpBody = bodyString.data(using: .utf8)
        
        let config : URLSessionConfiguration = URLSessionConfiguration.default
        let urlSession : URLSession = URLSession(configuration: config)
        let task = urlSession.dataTask(with: request as URLRequest, completionHandler: {
            (data, response, error) in
            if error == nil {
                let parsedData = try? JSONSerialization.jsonObject(with: data!) as! [String:Any]
                accessToken  = parsedData?["access_token"] as! String
                //print("Access Token \(accessToken)")
                self.delegate?.getAccessToken(accessToken)
            }
        })
        
        task.resume()
    }
    
    public func requestTimeLine(_ accessTokent : String , _ homeTimeLineURL : String,_ callBackTweetList: @escaping ([Tweet]) -> Void){
        let request : NSMutableURLRequest = NSMutableURLRequest()
        request.httpMethod = "GET"
        request.url = URL(string: homeTimeLineURL)!
        request.addValue(String.init(format: "Bearer %@", accessTokent), forHTTPHeaderField: "Authorization")
        request.addValue("application/json;charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.addValue("gzip", forHTTPHeaderField: "Accept-Encoding")
        let config : URLSessionConfiguration = URLSessionConfiguration.default
        let urlSession : URLSession = URLSession(configuration: config)
        let task = urlSession.dataTask(with: request as URLRequest, completionHandler: {
            (data, response, error) in
            if error == nil {
                
                let parsedData = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as! [[String:Any]]
                
               self.createTweetList(parsedData!)
                
                print("\n________HAZO_________\n")
                //print(self.tweetList)
                callBackTweetList(self.tweetList)
            }
        })
        
        task.resume()
    }

    private func createTweetList(_ parsedData: [[String:Any]]) {
        for tweetItem in parsedData{
           
            var name: String = ""
            var screenName: String = ""
            var profileImageURL: String = ""
            var descriptionText: String = ""
            
            if((tweetItem["retweeted_status"]) != nil){
                
                print("\n ------------------")
                    for item2 in (tweetItem["retweeted_status"]! as! [String:Any]){
                        switch item2 {
                        case ("text",_):
                            print(item2.value)
                            descriptionText = item2.value as! String
                            break
                        case ("user",_):
                            for item3 in (item2.value as! [String:Any]){
                                switch item3 {
                                case ("name",_) :
                                    print(item3.value)
                                    name = item3.value as! String
                                    break
                                case ("screen_name",_) :
                                    print(item3.value)
                                    screenName = item3.value as! String
                                    break
                                case ("profile_image_url_https",_) :
                                    print(item3.value)
                                    profileImageURL = item3.value as! String
                                    break
                                default :
                                    break
                                }
                            }
                            break
                        default :
                            break
                        }
                    }
                let tweet : Tweet = Tweet(name, screenName, profileImageURL, descriptionText)
                tweetList.append(tweet)
                
            }else{
                print("\n -----------------")
                //print(tweetItem)
                print(tweetItem["text"]!)
                descriptionText = tweetItem["text"] as! String
                for item in (tweetItem["user"]! as! [String:Any]){
                    switch item {
                    case ("name",_) :
                        print(item.value)
                        name = item.value as! String
                        break
                    case ("screen_name",_) :
                        print(item.value)
                        screenName = item.value as! String
                        break
                    case ("profile_image_url_https",_) :
                        print(item.value)
                        profileImageURL = item.value as! String
                        break
                    default :
                        break
                    }

                }
                let tweet : Tweet = Tweet(name, screenName, profileImageURL, descriptionText)
                tweetList.append(tweet)
            }
        }
    }

}
