//
//  ViewController.swift
//  TwitterTimeline
//
//  Created by Nhan Phung on 11/27/16.
//  Copyright © 2016 Nhan Phung. All rights reserved.
//

import UIKit

class ViewController: UIViewController{
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var countTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        userNameTextField.text = "@mojombo"
        countTextField.text = "20"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let info: (user: String ,count: String) = (userNameTextField.text! ,countTextField.text!)
        let timeLineView: TimelineViewController = segue.destination as! TimelineViewController
        timeLineView.info = info
    }

}

